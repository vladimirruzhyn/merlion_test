<?php

function __autoload($className) {
    $className = ltrim($className, '\\');
    $lastNsPos = strrpos($className, '\\');
    $namespace = substr($className, 0, $lastNsPos);
    $className = substr($className, $lastNsPos + 1);
    $namespacePath  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace).DIRECTORY_SEPARATOR;
    $fileName = $namespacePath.str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    if (file_exists($fileName)) {
        include_once($fileName);
    } else {
        $result = array(
            'result' => false, 
            'msg' => 'Класс "'.$className.'" не найдено по пути "'.$fileName.'"'
        );
        echo json_encode($result);
        exit;
    }
}

use \src\Dictionary;
use \src\Graph;
use \src\Calculator;

try {
    $dictionaryClass = new Dictionary();

    $origin = $_POST['start'];
    $destination = $_POST['end'];
    $useDictionary = $_POST['dictionary'];

    $result = $dictionaryClass->checkWord($origin);
    if ($result['result']) {
        $result = $dictionaryClass->checkWord($destination);
        if ($result['result']) {

            switch ($useDictionary) {
                case 'no':
                    $dictionary = $dictionaryClass->getDictionary();
                    $graph = Graph::getGraphFromDictionari($dictionary);
                    break;
                    
                default:
                    $graph = Graph::getGraphFromFile();
                    break;
            }

            $result = Calculator::calculateSequenceBFS(
                $origin, 
                $destination, 
                $graph['graph'], 
                $graph['visited']
            );

        }
    }
} catch (Exception $e) {
    $result = array('result' => false, 'msg' => $e->getMessage());
}
echo json_encode($result);
exit;