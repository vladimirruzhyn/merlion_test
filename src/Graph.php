<?php
/*
 *
 * Class of graph formation
 *
 * @package files
 *
 */

namespace src;

class Graph
{

    /**
     *
     * Load a serialized graph from a file
     *
     * @param string $path path to a file with a serialized graph
     *
     * @return array
     *
     */

    public static function getGraphFromFile($path = 'dictionary/graph.txt')
    {
        $result = array('graph'=>array(), 'visited'=>array());

        $serializeGraph = file_get_contents($path);
        if ($serializeGraph) {
            $result['graph'] = unserialize($serializeGraph);
            foreach ($result['graph'] as $key => $value) {
                $result['visited'][$key] = false;
            }
        } else {
            throw new Exception('Файл '.$path.' не найден.');
        }
        return $result;
    }

    /**
     *
     * Forms a graph from a one-dimensional array of words
     *
     * @param array $dictionary A set of words in the form of a one-dimensional array
     * @param int $count Number of characters in words
     *
     * @return array
     *
     */

    public static function getGraphFromDictionari($dictionary, $count = 8)
    {
        $result = array('graph'=>array(), 'visited'=>array());

        foreach ($dictionary as $key => $value) {
            $result['graph'][$value] = array();
            $result['visited'][$value] = false;
            for ($i=0; $i < $count; $i = $i + 2) { 
                $patern = substr_replace($value, '[a-я]{1}.', $i, 2);
                $wordGraph = preg_grep("/".$patern."/", $dictionary);
                $wordGraph = preg_grep("/".$value."/", $wordGraph, PREG_GREP_INVERT);
                foreach ($wordGraph as $index => $data) {
                    $result['graph'][$value][] = $data;
                }
            }
        }

        return $result;
    }
}