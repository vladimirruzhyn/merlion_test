<?php   
/*
 *
 * Class to work with the dictionary
 *
 */

namespace src;

class Dictionary
{

    function __construct($path = 'dictionary/dictionary.txt') 
    {
        $dictionary = file_get_contents($path);
        if ($dictionary) {
            $this->dictionary = explode(' ', $dictionary);
        } else {
            throw new Exception('Файл '.$path.' не найден.');
        }
    }

    /**
     *
     * Returns a one-dimensional array of words
     *
     * @return array
     *
     */

    public function getDictionary()
    {
        return $this->dictionary;
    }

    /**
     *
     * Checks for a word in the dictionary
     *
     * @param string $word сheck word
     *
     * @return array
     *
     */

    public function checkWord($word)
    {

        $wordUpper = mb_strtoupper($word);

        $result = array();
        $result['result'] = true;

        if (!in_array($wordUpper, $this->dictionary)) {
            $result['result'] = false;
            $result['msg'] = "Слово '".$word;
            $result['msg'] .= "' отсутствует в словаре. Введите другое слово";
        }

        return $result;
    }

}