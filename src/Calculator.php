<?php
/*
 * Class performs the sequence calculation
 *
 */

namespace src;

class Calculator
{
    /**
     *
     * Calculating the smallest number of conversions between words $origin and $destination
     *
     * @param string $origin starting word
     * @param string $destination ending word
     * @param array $graph graph
     * @param array $visited visited array
     *
     * @return array
     *
     */

    public static function calculateSequenceBFS($origin, $destination, $graph, $visited)
    {
        $origin = mb_strtoupper($origin);
        $destination = mb_strtoupper($destination);

        $q = new \SplQueue();
         

        $q->enqueue($origin);
         
        //Create a list to save the path
        $path = array();
        $path[$origin] = new \SplDoublyLinkedList();
        $path[$origin]->setIteratorMode(
            \SplDoublyLinkedList::IT_MODE_FIFO|\SplDoublyLinkedList::IT_MODE_KEEP
        );

        $path[$origin]->push($origin);

        //Add words to the path until we reach the final word
        while (!$q->isEmpty() && $q->bottom() != $destination) {
            $t = $q->dequeue();

            if (!empty($graph[$t])) {

                //Add neighboring nodes to the path and mark them as visited
                foreach ($graph[$t] as $vertex) {           
                    if (!$visited[$vertex]) {

                        $q->enqueue($vertex);
                        $visited[$vertex] = true;
  
                        $path[$vertex] = clone $path[$t];
                        $path[$vertex]->push($vertex);
                    }
                }
            }
        }

        $result = array();
        if (isset($path[$destination])) {
            $result['result'] = true;
            $msg =  "из '$origin' в '$destination' за ";
            $msg .= (count($path[$destination]) - 1);
            $msg .= " преобразование(ий/ия) ";
            $sep = '';
            foreach ($path[$destination] as $vertex) {
                $msg .= $sep. $vertex;
                $sep = '->';
            }
            $result['msg'] = $msg;
        } else {
            $result['result'] = false;
            $result['msg'] = "Нет пути из '$origin' в '$destination'";
        }
        return $result;
    }
}